class ManagePassord {

    static onLoad = ()=>{};

    static passToUrl(pass){
        return `//${pass}`;
    }

    static async CheckPassword(pass){
        return await new Promise(res => fetch(ManagePassord.passToUrl(pass) + '/my-folder-online').then(x => x.text().then(x => res(x === 'ok'))).catch(() => res(false)));
    }

    static LoadPassword(){
        const pass = sessionStorage.getItem("password") || localStorage.getItem("password");
        return ManagePassord.passToUrl(pass);
    }

    static CheckStoredPassword(){
        return ManagePassord.CheckPassword(ManagePassord.LoadPassword())
    }

    static async setPassword(pass, remember){
        const truePass = await ManagePassord.CheckPassword(pass);
        if(!truePass){
            return false;
        }

        if(remember){
            localStorage.setItem("password", pass);
        } else {
            sessionStorage.setItem("password", pass);
        }
        
        ManagePassord.onLoad();

        return true;
    }

    static Logout(){
        localStorage.removeItem("password")
        sessionStorage.removeItem("password");
    }
}

export default ManagePassord;