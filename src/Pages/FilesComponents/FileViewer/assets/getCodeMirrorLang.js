import * as map from 'lang-map';

const reMap = {
    html: 'htmlembedded'
}

export default function (ext){
    const lang = map.languages(ext)?.[0];

    if(lang){
        return reMap[lang] ?? lang;
    }
}