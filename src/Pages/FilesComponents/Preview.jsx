import FileViewer from './FileViewer/components/file-viewer';
import { Component } from 'react';
import { v4 } from 'uuid';
import * as bootstrap from 'bootstrap';

import "../../files/preview.sass";

export default class Preview extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: v4(),
            maxHeight: 200,
            show: false
        }

        this.getType = this.getType.bind(this);
    }

    UpdateModelSize() {
        this.setState({
            maxHeight: window.innerHeight * 0.70
        });
    }

    componentDidUpdate(prevProps) {
        if (this.props.filePath && this.props.copyId != prevProps.copyId) {
            setTimeout(() => this.openModel());
        }
    }

    componentDidMount() {
        const base = this;
        this.modelId = document.getElementById("model" + this.state.id);
        this.model = new bootstrap.Modal(this.modelId);
        this.modelId.addEventListener('hidden.bs.modal', function () {
            setTimeout(() => base.setState({ show: false }), 1000 * .15);
        });

        window.addEventListener('resize', () => this.UpdateModelSize());
        setTimeout(()=>this.UpdateModelSize());
    }

    openModel() {
        this.setState({ show: true });
        this.model.show();
    }

    getType() {
        return this.props.fileName.substring(this.props.fileName.lastIndexOf('.') + 1);
    }

    render() {
        return <>
            <div className="modal fade" tabIndex="-1" id={"model" + this.state.id} aria-labelledby={this.state.id} aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id={this.state.id} style={{ overflowWrap: 'anywhere' }}>File preview - {this.props.fileName}</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body" style={{ minHeight: '300px', maxHeight: this.state.maxHeight + 'px', overflow: 'auto' }}>
                            {this.state.show ? <FileViewer fileType={this.getType()} height={this.state.maxHeight - 250} width={200} filePath={this.props.filePath} />: null}
                        </div>
                    </div>
                </div>
            </div>
        </>
    }
}