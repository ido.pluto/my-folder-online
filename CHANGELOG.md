# My folder online - Desktop Version History

## v0.1.0

### Features
- 2 Servers supported: ngrok.com, anttunnel.com
- Option to specify the domain
- Setting option: Limit search results
- Setting option: Limit creation of zips
- Show logs in real-time
- Option to fast login link